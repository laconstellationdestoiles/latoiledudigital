+++
title = 'Architecture matérielle'
date = 2024-01-04T13:38:04+01:00
draft = false
+++

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Schenker_VIA14_Laptop_asv2021-01.jpg/400px-Schenker_VIA14_Laptop_asv2021-01.jpg" attr="Photo by A.Savin, WikiCommons" align=center title="Ordinateur">}}
L'ordinateur personnel (ou Personal Computer) se répand à partir des années 1980, aussi bien chez les particuliers que dans le milieu professionnel. 

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Smartphone_Use.jpg/400px-Smartphone_Use.jpg" align=center title="Smartphone">}}

Bien plus qu’un téléphone, le smartphone est un ordinateur de poche.
Il se déploie très rapidement dans la société à partir du lancement du premier iPhone en 2007. 
