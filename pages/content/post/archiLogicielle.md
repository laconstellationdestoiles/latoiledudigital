+++
title = 'Architecture logicielle'
date = 2024-01-04T13:38:04+01:00
draft = false
+++

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/A_schematic_of_WackoWiki_r6.0_database_structure.png/800px-A_schematic_of_WackoWiki_r6.0_database_structure.png" attr="wikimediacommons>EoNy CC BY-SA" align=center title="Données">}}
Les données, le contenu d'Internet, constituent le nouvel or noir du capitalisme numérique, base du modèle économique du Web fondé sur la prédiction et la publicité ciblée.

Le Réglement Général sur la Protection des Données (RGPD) du parlement européen limite la captation et l'exploitation des données personnelles par les entreprises.

Les États, et en particulier la NSA (Nationalité Security Agency) américaine considèrent leur collecte comme un enjeu stratégique.

Les bases de données massives nourrissent les modèles de langages (LLM) des IA génératives. 

